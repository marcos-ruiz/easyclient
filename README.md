Easyclient
==========

###Instalar Dependencias

Instalar NVM [Node Version Manager](https://github.com/creationix/nvm)

Se o SO for Windows abra o terminal como Administrador e execute o comando
```
nvm install latest
```

Execute o seguinte comando para definir a versão do NodeJs que acabou de instalar
```
nvm use version
```

Instalar `hotnode` and `http-server` 
```
npm i -g hotnode http-server
```

###Run application

Na raiz do projeto executar o comando abaixo para subir o `server` no `http://localhost:8081`
```
npm run serve
```

Para subir o `client` execute os seguintes comando em outro terminal
```
cd client
http-server -p 3000 -P 8081
```

###Observações

Todas as rotas da aplications estão configurada para o endereço abaixo com o `CORS` habilitado:
```
http://localhost:8081/api/v1
```