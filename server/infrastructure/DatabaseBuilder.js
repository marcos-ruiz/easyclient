const firebase = require('firebase');

class DatabaseBuilder {

    constructor() {
        this.reference = null;
    }

    ref(ref) {
        this.reference = ref;
        return this;
    }

    build() {
        firebase.initializeApp(this.connect);
        return firebase.database().ref(this.reference);
    }

    get connect() {
        return {
            apiKey: "AIzaSyBbN8sxrxtA7U0oXiytMpH3mu6E94MorZ8",
            authDomain: "blazing-fire-3497.firebaseapp.com",
            databaseURL: "https://blazing-fire-3497.firebaseio.com",
            storageBucket: "blazing-fire-3497.appspot.com",
        }
    }
}

module.exports = DatabaseBuilder;