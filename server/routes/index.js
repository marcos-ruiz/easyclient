// BASE SETUP
// =============================================================================

// call the packages we need
const express = require('express');

// ROUTES FOR OUR API
// =============================================================================
let router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api/v1)
router.get('/', function(req, res) {
    res.json({
        "/api" : "http://localhost:8080/api/v1",
        "/api/sample": "http://localhost:8080/api/v1/sample"
    });   
});

module.exports = router;