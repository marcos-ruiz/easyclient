// BASE SETUP
// =============================================================================

// call the packages we need
const express = require('express');
const DatabaseBuilder = require('../infrastructure/DatabaseBuilder');

// ROUTES FOR OUR API
// =============================================================================
let router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api/sample)
router.get('/', function(req, res) {
    
    let databaseBuilder = new DatabaseBuilder();
    let form = databaseBuilder.ref('form').build();

    form.on('value', (snapshot) => {
        res.status(200).json({'form': snapshot.val()});
    }, (error) => {
        res.status(404).json({'form': error.code});
    });
});

module.exports = router;