const BASE_URL = 'http://localhost:8081/api/v1/';

class Api
{
    get routers() 
    {
        return fetch(BASE_URL, { method: 'GET'});
    }
}

let api = new Api();
api.routers
    .then(response => response.json())
    .then(json => console.log(json))
    .catch(error => console.log(error));